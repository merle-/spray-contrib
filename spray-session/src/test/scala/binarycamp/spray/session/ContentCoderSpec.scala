package binarycamp.spray.session

import org.scalatest.FunSpec
import org.scalatest.Matchers._

class ContentCoderSpec extends FunSpec {
  val PlainSession = Map("attr1" -> "value1", "attr2" -> "value2")
  val EncodedPlainSession = "attr1=value1&attr2=value2"
  val Utf8Session = Map("attr1" -> "value 1", "attr 2" -> "=&č")
  val EncodedUtf8Session = "attr1=value+1&attr+2=%3D%26%C4%8D"

  describe("PlainCoder") {
    it("should encode session data containing ASCII characters") {
      val encoded = PlainCoder.encode(PlainSession)
      encoded should be(EncodedPlainSession)
    }

    it("should encode session data containing UTF-8 characters") {
      val encoded = PlainCoder.encode(Utf8Session)
      encoded should be(EncodedUtf8Session)
    }

    it("should decode previously encoded session data") {
      val encoded = PlainCoder.encode(Utf8Session)
      val decoded = PlainCoder.decode(encoded)
      decoded should be(Utf8Session)
    }
  }

  describe("JavaxCryptoCoder") {
    val coder = new JavaxCryptoCoder("AES", "AES/ECB/PKCS5Padding", "secret", "PBKDF2WithHmacSHA1", 1, 128)

    it("should decode previously encoded session data") {
      val encoded = coder.encode(Utf8Session)
      val decoded = coder.decode(encoded)
      decoded should be(Utf8Session)
    }
  }
}
