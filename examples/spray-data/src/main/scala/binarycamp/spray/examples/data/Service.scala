package binarycamp.spray.examples.data

import akka.actor.{ Actor, Props }
import binarycamp.spray.common.Error
import binarycamp.spray.common.Result.Response
import binarycamp.spray.data.{ Page, PagePimps, PageRequest }
import binarycamp.spray.examples.data.Errors._

class Service extends Actor with PagePimps {
  import binarycamp.spray.examples.data.Service._

  var books = List(
    Book("1", "Pride and Prejudice", "Jane Austen"),
    Book("2", "Jane Eyre", "Charlotte Brontë"),
    Book("3", "Wuthering Heights", "Emily Brontë"),
    Book("4", "The Picture of Dorian Gray", "Oscar Wilde"),
    Book("5", "Alice's Adventures in Wonderland & Through the Looking-Glass", "Lewis Carroll"),
    Book("6", "Crime and Punishment", "Fyodor Dostoyevsky"),
    Book("7", "Frankenstein", "Mary Shelley"),
    Book("8", "Emma", "Jane Austen"),
    Book("9", "The Count of Monte Cristo", "Alexandre Dumas"),
    Book("10", "Persuasion", "Jane Austen"))

  var nextBookId = 11

  override def receive: Receive = {
    case GetBook(bookId)       ⇒ sender ! getBook(bookId)
    case AddBook(newBook)      ⇒ sender ! addBook(newBook)
    case GetBooks(pageRequest) ⇒ sender ! getBooks(pageRequest.getOrElse(PageRequest(0, 5, None, None)))
  }

  def getBook(bookId: String): GetBookResponse =
    GetBookResponse(books.find(_.id == bookId).toRight(BookNotFound(bookId)))

  def addBook(newBook: NewBook): AddBookResponse =
    if (books.exists(_.name == newBook.name)) AddBookResponse(Left(DuplicateBook(newBook.name)))
    else {
      val book = Book(nextBookId.toString, newBook.name, newBook.author)
      books = books :+ book
      nextBookId += 1
      AddBookResponse(Right(book))
    }

  def getBooks(pageRequest: PageRequest): GetBooksResponse = GetBooksResponse(Right(books.page(pageRequest, 5)))
}

object Service {
  case class GetBook(bookId: String)
  case class GetBookResponse(result: Either[Error, Book]) extends Response[Book]

  case class AddBook(newBook: NewBook)
  case class AddBookResponse(result: Either[Error, Book]) extends Response[Book]

  case class GetBooks(pageRequest: Option[PageRequest])
  case class GetBooksResponse(result: Either[Error, Page[Book]]) extends Response[Page[Book]]

  def props: Props = Props[Service]
}
