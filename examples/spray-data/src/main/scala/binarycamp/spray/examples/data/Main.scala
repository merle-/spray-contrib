package binarycamp.spray.examples.data

import akka.actor.ActorSystem
import akka.pattern.ask
import akka.util.Timeout
import binarycamp.spray.common._
import binarycamp.spray.data.PageDirectives.MalformedPageRequestRejection
import binarycamp.spray.data._
import binarycamp.spray.examples.data.Errors._
import binarycamp.spray.examples.data.Service.{ AddBookResponse, GetBookResponse, GetBooksResponse }
import scala.concurrent.duration._
import scala.util.control.NonFatal
import spray.http.{ LanguageRange, StatusCodes, Uri }
import spray.routing._

object Main extends App with SimpleRoutingApp with LanguageDirectives with PageDirectives with JsonSupport {
  implicit val system = ActorSystem("example")

  import system.dispatcher

  implicit val timeout = Timeout(1.seconds)

  implicit val errorTranslator = new ToMarshallableErrorTranslator {
    val ms = new ResourceMessageSource("Messages")

    private val links = LinkProvider[Error] { error ⇒
      Some(Seq(Link(Uri("http://localhost:8080/errors/" + error.code))))
    }

    override def apply(error: Error, languages: Seq[LanguageRange]): MarshallableError =
      ms(error.code, Nil, LanguagePolicy.Default(languages)) match {
        case Right(msg) ⇒ MarshallableError(error.code, msg, None, links(error))
        case Left(_)    ⇒ MarshallableError(error.code, error.code, None, links(error))
      }
  }

  implicit val statusCodeMapper = ErrorToStatusCodeMapper {
    case _: BookNotFound    ⇒ StatusCodes.NotFound
    case _: DuplicateBook   ⇒ StatusCodes.BadRequest
    case InvalidPageRequest ⇒ StatusCodes.BadRequest
  }

  val exceptionHandler = ExceptionHandler {
    case NonFatal(_) ⇒ completeWithLanguage(UnknownError)
  }

  val rejectionHandler = RejectionHandler {
    case MalformedPageRequestRejection(_, _) :: _ ⇒ completeWithLanguage(InvalidPageRequest)
    case _                                        ⇒ completeWithLanguage(UnknownError)
  }

  val handleErrors = handleExceptions(exceptionHandler) & handleRejections(rejectionHandler)

  startServer("localhost", 8080) {
    handleErrors {
      pathPrefix("api") {
        pathPrefix("books") {
          pathEnd {
            getWithPageRequest { pageRequest ⇒
              completeWithLanguage(getBooks(pageRequest))
            } ~ post {
              entity(as[NewBook]) { book ⇒
                completeWithLanguage((StatusCodes.Created, addBook(book)))
              }
            }
          } ~ pathPrefix(Segment) { bookId ⇒
            completeWithLanguage(getBook(bookId))
          }
        }
      } ~ get {
        pathEndOrSingleSlash {
          getFromResource("index.html")
        } ~ getFromResourceDirectory("")
      }
    }
  }

  val service = system.actorOf(Service.props)

  def getBook(bookId: String) =
    service.ask(Service.GetBook(bookId)).mapTo[GetBookResponse]

  def addBook(book: NewBook) =
    service.ask(Service.AddBook(book)).mapTo[AddBookResponse]

  def getBooks(pageRequest: Option[PageRequest]) =
    service.ask(Service.GetBooks(pageRequest)).mapTo[GetBooksResponse].mapResult {
      _.asCollectionResource(Uri("http://localhost:8080/api/books"))
    }

  sys.addShutdownHook(system.shutdown())
}
