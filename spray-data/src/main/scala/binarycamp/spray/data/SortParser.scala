package binarycamp.spray.data

import binarycamp.spray.data.PageRequest._
import org.parboiled2._

private[data] class SortParser(val input: ParserInput) extends Parser {
  def sort: Rule1[Sort] = rule { oneOrMore(field).separatedBy(",") ~ EOI ~> (Sort(_: Seq[(String, Option[Order])])) }

  def field = rule { fieldName ~ optional(":" ~ order) ~> ((_: String, _: Option[Order])) }

  def fieldName = rule { capture(CharPredicate.Alpha ~ zeroOrMore(CharPredicate.AlphaNum)) }

  def order = rule { asc | desc }

  def asc = rule { ignoreCase(AscParam) ~ push(Order.Asc) }

  def desc = rule { ignoreCase(DescParam) ~ push(Order.Desc) }
}
