package binarycamp.spray.data

import spray.httpx.unmarshalling._
import scala.util.{ Failure, Success }
import org.parboiled2.ParseError

trait PageRequestDeserializers {
  implicit object SortDeserializer extends Deserializer[Seq[String], Sort] {
    def apply(values: Seq[String]): Deserialized[Sort] =
      values.foldLeft[Deserialized[Sort]](Right(Sort())) {
        case (acc, value) ⇒ for (a ← acc.right; b ← parse(value).right) yield a.merge(b)
      }

    private def parse(value: String): Deserialized[Sort] = {
      val parser = new SortParser(value)
      parser.sort.run() match {
        case Success(sort)          ⇒ Right(sort)
        case Failure(e: ParseError) ⇒ Left(MalformedContent(parser.formatError(e), e))
        case Failure(e)             ⇒ Left(MalformedContent("Error parsing sort", e))
      }
    }
  }

  implicit object FilterDeserializer extends Deserializer[String, Filter] {
    def apply(value: String): Deserialized[Filter] = Right(Filter(value))
  }

  implicit object PageRequestDeserializer extends Deserializer[Map[String, List[String]], Option[PageRequest]] {
    override def apply(params: Map[String, List[String]]): Deserialized[Option[PageRequest]] = {
      val parsed = for {
        page ← readOpt[String, Int](params.get(PageRequest.IndexParam).flatMap(_.headOption)).right
        size ← readOpt[String, Int](params.get(PageRequest.SizeParam).flatMap(_.headOption)).right
        sort ← readOpt[Seq[String], Sort](params.get(PageRequest.SortParam).map(_.reverse)).right
        filter ← readOpt[String, Filter](params.get(PageRequest.FilterParam).flatMap(_.headOption)).right
      } yield (page, size, sort, filter)

      parsed match {
        case Right((None, None, None, None))   ⇒ Right(None)
        case Right((page, size, sort, filter)) ⇒ Right(Some(PageRequest(page.getOrElse(0), size, sort, filter)))
        case Left(error)                       ⇒ Left(error)
      }
    }

    private def readOpt[A, B](value: Option[A])(implicit d: Deserializer[Option[A], Option[B]]) = d(value)
  }
}

object PageRequestDeserializers extends PageRequestDeserializers {
  def read[A](value: A)(implicit d: Deserializer[A, Option[PageRequest]]): Deserialized[Option[PageRequest]] = d(value)
}
