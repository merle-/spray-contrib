package binarycamp.spray

import binarycamp.spray.common.ErrorTranslator
import shapeless.HNil

package object data {
  type ToMarshallableErrorTranslator = ErrorTranslator[MarshallableError]

  type ResourceLocator0 = ResourceLocator[HNil]
}
