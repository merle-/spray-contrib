package binarycamp.spray.data

case class MarshallableError(code: String, message: String, messages: Option[Seq[String]], links: Option[Seq[Link]])
