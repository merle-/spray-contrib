package binarycamp.spray.data

object DefaultFieldNames {
  // format: OFF
  val Links     = "@links"
  val LinkHref  = "href"
  val LinkTitle = "title"

  val PageItems  = "items"
  val TotalCount = "count"

  val Error         = "@error"
  val ErrorCode     = "@code"
  val ErrorMessage  = "@message"
  val ErrorMessages = "@messages"
}
