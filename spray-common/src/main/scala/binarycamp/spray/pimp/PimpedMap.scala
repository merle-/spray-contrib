package binarycamp.spray.pimp

import scala.collection.GenTraversableOnce

class PimpedMap[A, B](val map: Map[A, B]) extends AnyVal {
  def +?[B1 >: B](kv: (A, Option[B1])): Map[A, B1] = if (kv._2.isDefined) map + (kv._1 -> kv._2.get) else map

  def ++?[B1 >: B](kvs: GenTraversableOnce[(A, Option[B1])]): Map[A, B1] =
    map ++ kvs.toSeq.filter(_._2.isDefined).map(kv ⇒ kv._1 -> kv._2.get)
}
