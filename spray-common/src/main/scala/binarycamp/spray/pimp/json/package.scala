package binarycamp.spray.pimp

import scala.language.implicitConversions
import spray.json.JsObject

package object json {
  implicit def pimpJsObject(jsObject: JsObject): PimpedJsObject = new PimpedJsObject(jsObject)
}
