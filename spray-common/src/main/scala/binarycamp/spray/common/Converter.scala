package binarycamp.spray.common

trait Converter[A, B] extends (A ⇒ B)

object Converter
  extends ToStringSerializers
  with ParamMapWriters
