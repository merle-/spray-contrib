package binarycamp.spray.common

import scala.language.implicitConversions
import spray.http.StatusCode
import spray.http.StatusCodes.InternalServerError

trait ErrorToStatusCodeMapper extends ErrorToStatusCodeMapper.PF

object ErrorToStatusCodeMapper {
  type PF = PartialFunction[Error, StatusCode]

  def apply(f: PF): ErrorToStatusCodeMapper = new ErrorToStatusCodeMapper {
    override def apply(error: Error): StatusCode = f(error)
    override def isDefinedAt(error: Error): Boolean = f.isDefinedAt(error)
  }

  implicit def toErrorCodeMapper(f: PF): ErrorToStatusCodeMapper = apply(f)

  implicit val Default = ErrorToStatusCodeMapper { case _ ⇒ InternalServerError }
}
