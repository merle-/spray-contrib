package binarycamp.spray.common

trait AdditionalMarshallers
  extends StatusCodeMarshallers
  with ErrorMarshallers
  with ResultMarshallers
