package binarycamp.spray.common

import java.util.Locale
import scala.annotation.tailrec
import spray.http.{ Language, LanguageRange, LanguageRanges }

trait LanguagePolicy extends (Seq[LanguageRange] ⇒ Seq[Locale])

object LanguagePolicy {
  def apply(f: Seq[LanguageRange] ⇒ Seq[Locale]): LanguagePolicy = new LanguagePolicy {
    override def apply(languages: Seq[LanguageRange]): Seq[Locale] = f(languages)
  }

  implicit val Default = new LanguagePolicy {
    override def apply(languages: Seq[LanguageRange]): Seq[Locale] =
      expand {
        if (languages.isEmpty) Seq(defaultLocale)
        else languages.distinct.map {
          case language: Language    ⇒ asLocale(language)
          case LanguageRanges.`*`(_) ⇒ defaultLocale
        }
      }

    private def expand(locales: Seq[Locale]): Seq[Locale] = {
      @tailrec
      def expand(acc: Seq[Locale], locales: Seq[Locale]): Seq[Locale] = {
        val parents = locales.map(drop).flatten.diff(acc)
        if (parents.isEmpty) acc
        else expand(acc ++ parents, parents)
      }

      expand(locales, locales)
    }

    private def defaultLocale: Locale = Locale.getDefault(Locale.Category.FORMAT)

    private def asLocale(range: LanguageRange): Locale = range match {
      case Language(primaryTag, subTags, _) ⇒
        subTags.size match {
          case 0 ⇒ new Locale(primaryTag)
          case 1 ⇒ new Locale(primaryTag, subTags(0))
          case _ ⇒ new Locale(primaryTag, subTags(0), subTags(1))
        }
      case LanguageRanges.`*`(_) ⇒ defaultLocale
    }

    private def drop(locale: Locale): Option[Locale] =
      if (locale.getVariant != "") Some(new Locale(locale.getLanguage, locale.getCountry))
      else if (locale.getCountry != "") Some(new Locale(locale.getLanguage))
      else None
  }
}
