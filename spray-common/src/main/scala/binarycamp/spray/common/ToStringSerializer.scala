package binarycamp.spray.common

object ToStringSerializer {
  def apply[T](f: T ⇒ String): ToStringSerializer[T] = new ToStringSerializer[T] {
    override def apply(t: T): String = f(t)
  }

  def write[T](t: T)(implicit w: ToStringSerializer[T]): String = w(t)
}

object ToStringOptionSerializer {
  def apply[T](f: T ⇒ Option[String]): ToStringOptionSerializer[T] = new ToStringOptionSerializer[T] {
    override def apply(t: T): Option[String] = f(t)
  }

  def write[T](t: T)(implicit w: ToStringOptionSerializer[T]): Option[String] = w(t)
}

trait ToStringSerializers
    extends LowPriorityToStringOptionSerializers
    with BasicToStringSerializers {

  implicit def optionToStringOptionSerializer[T](implicit s: ToStringSerializer[T]): ToStringOptionSerializer[Option[T]] =
    ToStringOptionSerializer[Option[T]] {
      case Some(value) ⇒ Some(s(value))
      case None        ⇒ None
    }
}

private[common] trait LowPriorityToStringOptionSerializers {
  implicit def defaultToStringOptionSerializer[T](implicit s: ToStringSerializer[T]): ToStringOptionSerializer[T] =
    ToStringOptionSerializer[T] {
      case value ⇒ Some(s(value))
    }
}

trait BasicToStringSerializers {
  implicit object SymbolToStringSerializer extends ToStringSerializer[Symbol] {
    override def apply(value: Symbol): String = value.name
  }

  implicit object StringToStringSerializer extends ToStringSerializer[String] {
    override def apply(value: String): String = value
  }

  implicit object IntToStringSerializer extends ToStringSerializer[Int] {
    override def apply(value: Int): String = value.toString
  }

  implicit object LongToStringSerializer extends ToStringSerializer[Long] {
    override def apply(value: Long): String = value.toString
  }

  implicit object ShortToStringSerializer extends ToStringSerializer[Short] {
    override def apply(value: Short): String = value.toString
  }

  implicit object ByteToStringSerializer extends ToStringSerializer[Byte] {
    override def apply(value: Byte): String = value.toString
  }
}
