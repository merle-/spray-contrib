package binarycamp.spray.common

import scala.language.implicitConversions
import scala.util.Either

sealed abstract class Result[+R] {
  def toEither: Either[Error, R]
}

object Result {
  abstract class Response[+R] extends Result[R] {
    def result: Either[Error, R]
    final override def toEither: Either[Error, R] = result
  }

  abstract class Failure extends Result[Nothing] {
    def error: Error
    final override def toEither: Either[Error, Nothing] = Left(error)
  }

  abstract class Success[+R] extends Result[R] {
    def result: R
    final override def toEither: Either[Error, R] = Right(result)
  }

  implicit def resultToEither[R](result: Result[R]): Either[Error, R] = result.toEither
}
