package binarycamp.spray.common

object ParamMapWriter {
  def apply[T](f: T ⇒ Map[String, String]): ParamMapWriter[T] = new ParamMapWriter[T] {
    override def apply(t: T): Map[String, String] = f(t)
  }

  def write[T](t: T)(implicit w: ParamMapWriter[T]): Map[String, String] = w(t)

  def field[T](name: String, value: T)(implicit s: ToStringOptionSerializer[T]): List[(String, String)] =
    s(value).map(name -> _ :: Nil).getOrElse(Nil)
}

trait ParamMapWriters {
  implicit object MapParamMapWriter extends ParamMapWriter[Map[String, String]] {
    override def apply(map: Map[String, String]): Map[String, String] = map
  }

  implicit def optionParamMapWriter[T](implicit w: ParamMapWriter[T]): ParamMapWriter[Option[T]] =
    ParamMapWriter[Option[T]] {
      case Some(value) ⇒ w(value)
      case None        ⇒ Map.empty
    }
}
