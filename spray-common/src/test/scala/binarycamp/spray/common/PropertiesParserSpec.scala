package binarycamp.spray.common

import org.parboiled2.ParseError
import org.scalatest.FlatSpec
import org.scalatest.Matchers._

class PropertiesParserSpec extends FlatSpec {
  def parseProps(input: String): Properties = new PropertiesParser(input).Properties.run().get

  "PropertiesParser" should "parse input with valid key value pairs" in {
    val input =
      """
        |key1=value1
        |key2 = value2
        |key3 : value3
      """.stripMargin

    parseProps(input) should be(Map("key1" -> "value1", "key2" -> "value2", "key3" -> "value3"))
  }

  it should "parse input with line terminator escapes" in {
    val input =
      """
        |ke\
        |y1=value1
        |key2\
        |=value2
        |key3=val\
        |     ue3
      """.stripMargin

    parseProps(input) should be(Map("key1" -> "value1", "key2" -> "value2", "key3" -> "value3"))
  }

  it should "parse input with empty values" in {
    val input =
      """
        |key1
        |key2 =
        |key3 :
      """.stripMargin

    parseProps(input) should be(Map("key1" -> "", "key2" -> "", "key3" -> ""))
  }

  it should "parse input with blank lines" in {
    val input =
      """
        |
        |key1=value1
        |
        |
        |key2=value2
        |
        |key3=value3
        |
      """.stripMargin

    parseProps(input) should be(Map("key1" -> "value1", "key2" -> "value2", "key3" -> "value3"))
  }

  it should "parse input with comments" in {
    val input =
      """
        |# Comment
        |key1=value1
        |! Comment followed be a blank line
        |
        |key2=value2
        |
        |   # Padded comment
        |key3=value3
      """.stripMargin

    parseProps(input) should be(Map("key1" -> "value1", "key2" -> "value2", "key3" -> "value3"))
  }

  it should "parse input with escaped chars" in {
    val input =
      """
        |key\n1=val\rue1
        |key\t2=val\u00E1ue2
        |key\f3=value3
      """.stripMargin

    parseProps(input) should be(Map("key\n1" -> "val\rue1", "key\t2" -> "valáue2", "key\f3" -> "value3"))
  }

  it should "parse UTF8 input" in {
    parseProps("key=ľščťžýáíé") should be(Map("key" -> "ľščťžýáíé"))
  }

  it should "produce ParseError when key is missing" in {
    intercept[ParseError] {
      parseProps(" = value")
    }
  }
}
