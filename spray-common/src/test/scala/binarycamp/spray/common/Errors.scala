package binarycamp.spray.common

import spray.http.{ Language, StatusCodes }

object Errors {
  case object Group extends ErrorTag

  val EN = Language("en")
  val DE = Language("de")

  case object Error1 extends Error {
    override val code = "Error1"
  }

  case object Error2 extends Error {
    override val tags = List(Group)
    override val code = "Error1"
  }

  case object Error3 extends Error {
    override val code = "Error1"
  }

  val Error1Message_EN = "Error 1"
  val Error1Message_DE = "Fehler 1"
  val Error2Message_EN = "Error 2"
  val Error2Message_DE = "Fehler 2"

  val Error1StatusCode = StatusCodes.InternalServerError
  val Error2StatusCode = StatusCodes.NotImplemented
}
