package binarycamp.spray.common

import org.scalatest.FlatSpec
import org.scalatest.Matchers._
import spray.http.Uri
import spray.json._

class DefaultSprayJsonFormatsSpec extends FlatSpec with DefaultSprayJsonFormats {
  val uri = Uri("http://localhost:8080/api?search=abc#1")

  "UriJsonFormat" should "convert an Uri to a JsString" in {
    uri.toJson should be(JsString(uri.toString()))
  }

  it should "convert a JsString with a valid uri to an Uri" in {
    JsString(uri.toString()).convertTo[Uri] should be(uri)
  }

  it should "produce DeserializationException when converting a JsString with an invalid uri" in {
    intercept[DeserializationException] {
      JsString("<").convertTo[Uri]
    }
  }

  "DefaultSprayJsonFormats.field" should "return an empty field for None" in {
    field[Option[Int]]("name", None) should be(Nil)
  }

  it should "return a field for Option[Int]" in {
    field[Option[Int]]("name", Some(1)) should be(List("name" -> JsNumber(1)))
  }
}
