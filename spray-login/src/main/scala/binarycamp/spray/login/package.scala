package binarycamp.spray

import spray.routing.Route
import spray.routing.authentication.ContextAuthenticator

package object login {
  type LoginAuthenticator[U] = ContextAuthenticator[UserInfo[U]]
  type LoginPageRoute = Option[String] ⇒ Route
}
