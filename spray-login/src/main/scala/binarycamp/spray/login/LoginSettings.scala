package binarycamp.spray.login

import akka.actor.ActorRefFactory
import binarycamp.spray.pimp._
import com.typesafe.config.Config
import spray.http.Uri
import spray.http.Uri.Path
import spray.util._

case class LoginSettings(loginPath: Path, defaultReturnUri: Uri)

object LoginSettings extends SettingsCompanion[LoginSettings]("binarycamp.spray.login") {
  override def fromSubConfig(c: Config): LoginSettings =
    LoginSettings(
      c.getRelativeUriPath("login-path"),
      c.getUri("default-return-uri"))

  implicit def default(implicit refFactory: ActorRefFactory) = apply(actorSystem)
}
