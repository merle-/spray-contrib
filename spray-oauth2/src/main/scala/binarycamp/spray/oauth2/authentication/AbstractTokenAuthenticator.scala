package binarycamp.spray.oauth2.authentication

import binarycamp.spray.oauth2.common.AccessToken
import scala.concurrent.{ ExecutionContext, Future }
import spray.http.HttpChallenge
import spray.http.HttpHeaders.`WWW-Authenticate`
import spray.routing.AuthenticationFailedRejection.{ CredentialsMissing, CredentialsRejected }
import spray.routing._
import spray.routing.authentication._

abstract class AbstractTokenAuthenticator[U](authenticator: AccessTokenAuthenticator[U],
                                             scheme: String, realm: String)(implicit ec: ExecutionContext)
    extends ContextAuthenticator[ScopedUser[U]] {

  import binarycamp.spray.oauth2.authentication.AbstractTokenAuthenticator._

  override def apply(ctx: RequestContext): Future[Authentication[ScopedUser[U]]] =
    extractAccessToken(ctx) match {
      case Right(accessToken) ⇒ authenticator(accessToken).map {
        case Some(restrictedUser) ⇒ Right(restrictedUser)
        case None                 ⇒ Left(rejection(CredentialsRejected))
      }
      case Left(AccessTokenMissing) ⇒ Future.successful(Left(rejection(CredentialsMissing)))
      case Left(AccessTokenInvalid) ⇒ Future.successful(Left(rejection(CredentialsRejected)))
    }

  def extractAccessToken(ctx: RequestContext): Either[AccessTokenError, AccessToken]

  private def rejection(cause: AuthenticationFailedRejection.Cause): Rejection = {
    val header = `WWW-Authenticate`(HttpChallenge(scheme, realm, Map.empty)) :: Nil
    AuthenticationFailedRejection(cause, header)
  }
}

object AbstractTokenAuthenticator {
  sealed trait AccessTokenError
  case object AccessTokenMissing extends AccessTokenError
  case object AccessTokenInvalid extends AccessTokenError
}
