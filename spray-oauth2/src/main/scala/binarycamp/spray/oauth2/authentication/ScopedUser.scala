package binarycamp.spray.oauth2.authentication

final case class ScopedUser[U](user: U, scopes: Set[String])
