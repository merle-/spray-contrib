package binarycamp.spray.oauth2.common

import binarycamp.spray.common.DefaultHttpHeaders.NoCacheHeaders
import binarycamp.spray.common.DefaultSprayJsonFormats
import spray.http.StatusCodes.OK
import spray.httpx.SprayJsonSupport
import spray.httpx.marshalling.{ Marshaller, ToResponseMarshaller }
import spray.json._

trait TokenSprayJsonSupport extends DefaultSprayJsonFormats with SprayJsonSupport {
  implicit val TokenJsonFormat = new RootJsonFormat[TokenResponse] {
    override def read(json: JsValue): TokenResponse = json match {
      case JsObject(fields) ⇒
        TokenResponse(
          AccessToken(
            fromField[String](json, "token_type"),
            fromField[String](json, "access_token"),
            additionalAttributes(fields)),
          fromField[Option[String]](json, "refresh_token"),
          fromField[Long](json, "expires_in"),
          fromField[Option[String]](json, "scope").map(scopes ⇒ scopes.split(" ").toSet))
      case _ ⇒ deserializationError("Object expected")
    }

    private def additionalAttributes(fields: Map[String, JsValue]): Option[Map[String, String]] = {
      val map = fields.filterKeys(key ⇒ !AccessToken.ReservedKeys.contains(key)).map {
        case (key, JsString(value)) ⇒ key -> value
        case (key, _)               ⇒ deserializationError(s"Additional attribute $key must be a JsString")
      }
      if (map.isEmpty) None else Some(map)
    }

    override def write(token: TokenResponse): JsValue = {
      val fields = new collection.mutable.ListBuffer[(String, JsValue)]
      fields ++= field("access_token", token.accessToken.token)
      fields ++= field("token_type", token.accessToken.tokenType)
      fields ++= field("expires_in", token.expiresIn)
      fields ++= field("refresh_token", token.refreshToken)
      fields ++= field("scope", token.scopes.map(_.mkString(" ")))
      fields ++= token.accessToken.additionalAttributes.getOrElse(Nil).map(kv ⇒ kv._1 -> JsString(kv._2))
      JsObject(fields: _*)
    }
  }

  implicit def tokenToResponseMarshaller(implicit m: Marshaller[TokenResponse]): ToResponseMarshaller[TokenResponse] =
    ToResponseMarshaller.fromMarshaller[TokenResponse](status = OK, headers = NoCacheHeaders)
}

object TokenSprayJsonSupport extends TokenSprayJsonSupport
