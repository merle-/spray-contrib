package binarycamp.spray.oauth2.server

import binarycamp.spray.login.UserService
import binarycamp.spray.oauth2.authentication.{ AccessTokenAuthenticator, ScopedUser }
import binarycamp.spray.oauth2.common.AccessToken
import binarycamp.spray.oauth2.server.TokenService._
import scala.concurrent.{ ExecutionContext, Future }

class DefaultAccessTokenAuthenticator[U](tokenService: TokenService,
                                         userService: UserService[U])(implicit ec: ExecutionContext)
    extends AccessTokenAuthenticator[U] {

  override def apply(accessToken: AccessToken): Future[Option[ScopedUser[U]]] =
    tokenService.verifyToken(accessToken).flatMap {
      case Some(ScopedUserId(id, scopes)) ⇒ userService.findById(id).map(_.map(info ⇒ ScopedUser(info.user, scopes)))
      case None                           ⇒ Future.successful(None)
    }
}

object DefaultAccessTokenAuthenticator {
  def apply[U](tokenService: TokenService,
               userService: UserService[U])(implicit ec: ExecutionContext): DefaultAccessTokenAuthenticator[U] =
    new DefaultAccessTokenAuthenticator[U](tokenService, userService)
}
