package binarycamp.spray.oauth2.server

import binarycamp.spray.data.MarshallableError
import shapeless.{ ::, HNil }
import spray.http.Uri
import spray.routing.{ Directive, Directive1, Route }

trait AuthorizationRequestHandler extends RequestHandler {
  def canHandle(params: RequestParameters): Boolean
  def clientContext(params: RequestParameters): Directive[Client :: Uri :: HNil]
  def validate(params: RequestParameters): Directive1[RequestParameters]
  def handle(userId: String, client: Client, redirectionUri: Uri, scopes: Set[String], params: RequestParameters): Route
  def handleError(error: MarshallableError, redirectionUri: Uri, params: RequestParameters): Route
}
