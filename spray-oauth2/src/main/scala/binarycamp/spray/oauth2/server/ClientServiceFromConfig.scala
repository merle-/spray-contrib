package binarycamp.spray.oauth2.server

import binarycamp.spray.oauth2.server.ClientTypes._
import binarycamp.spray.pimp._
import com.typesafe.config.Config
import scala.collection.JavaConversions._
import scala.concurrent.{ ExecutionContext, Future }
import scala.util.control.NonFatal
import spray.http.Uri
import spray.routing.authentication._
import spray.util.LoggingContext

private[server] class ClientServiceFromConfig(config: Config)(implicit logger: LoggingContext) extends ClientService {
  private val clients = (for (id ← config.root.unwrapped().keys) yield id -> parseClient(id)).toMap

  private def parseClient(id: String): ClientWithSecret =
    try {
      // format: OFF
      val c               = config.getConfig(id)
      val name            = c.getString("name")
      val clientType      = ClientTypes.parse(c.getString("client-type"))
      val secret          = c.getOptString("secret")
      val hasCredentials  = clientType == Confidential || secret.isDefined
      val scopes          = c.getStringSet("scopes")
      val defaultScopes   = c.getOptStringSet("default-scopes")
      val policy          = c.getOptString("scope-policy").map(ScopePolicies.parse).getOrElse(ScopePolicies.FailOnInvalid)
      val redirectionUris = c.getStringList("redirection-uris").map(Uri(_))
      val needsApproval   = c.getBoolean("needs-approval")
      // format: ON
      ClientWithSecret(Client(id, name, clientType, hasCredentials, scopes, defaultScopes, policy, redirectionUris,
        needsApproval), secret)
    } catch { case NonFatal(e) ⇒ sys.error(s"Cannot read client $id") }

  def apply(id: String): Future[Option[Client]] = Future.successful(clients.get(id).map(_.client))

  def authenticator(implicit ec: ExecutionContext): ClientAuthenticator = {
    val authenticator: UserPassAuthenticator[Client] = {
      case Some(UserPass(id, secret)) ⇒ Future.successful(clients.get(id).filter(_.secret == Some(secret)).map(_.client))
      case None                       ⇒ Future.successful(None)
    }
    BasicAuth(authenticator, "Secured resource")
  }
}

private[server] case class ClientWithSecret(client: Client, secret: Option[String]) {
  require(!client.hasCredentials || secret.isDefined, s"Client ${client.id} has no secret")
}
