package binarycamp.spray.oauth2.server

import binarycamp.spray.oauth2.common.{ AccessToken, BearerToken }
import java.util.UUID

trait TokenGenerator {
  def accessToken: AccessToken
  def refreshToken: String
}

object TokenGenerator {
  implicit def default: TokenGenerator = BearerTokenGenerator()
}

final class BearerTokenGenerator extends TokenGenerator {
  override def accessToken: AccessToken = BearerToken(next)
  override def refreshToken: String = next

  private def next = UUID.randomUUID().toString
}

object BearerTokenGenerator {
  def apply(): BearerTokenGenerator = new BearerTokenGenerator
}
