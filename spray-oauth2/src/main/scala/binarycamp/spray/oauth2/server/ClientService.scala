package binarycamp.spray.oauth2.server

import com.typesafe.config.Config
import scala.concurrent.{ ExecutionContext, Future }
import spray.util.LoggingContext

trait ClientService extends (String ⇒ Future[Option[Client]]) {
  def authenticator(implicit ec: ExecutionContext): ClientAuthenticator
}

object ClientService {
  implicit def default(implicit settings: ServerSettings, logger: LoggingContext): ClientService =
    apply(settings.clients)

  def apply(config: Config)(implicit logger: LoggingContext): ClientService = new ClientServiceFromConfig(config)
}
