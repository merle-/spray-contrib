import com.typesafe.sbt.SbtScalariform
import com.typesafe.sbt.SbtScalariform.ScalariformKeys
import play.twirl.sbt.SbtTwirl
import sbt.Keys._
import sbt._

object Build extends Build {
  lazy val buildSettings = Seq(
    organization := "com.binarycamp.spray-contrib",
    version := "0.1.0-SNAPSHOT",
    scalaVersion := "2.11.4")

  lazy val root = Project("spray-contrib", file(".")).
    aggregate(common, data, session, login, oauth2, examples).
    settings(rootSettings: _*)

  lazy val common = Project("spray-common", file("spray-common")).
    settings(moduleSettings: _*).
    settings(libraryDependencies ++= Dependencies.common)

  lazy val data = Project("spray-data", file("spray-data")).
    dependsOn(common).
    settings(moduleSettings: _*).
    settings(libraryDependencies ++= Dependencies.data)

  lazy val session = Project("spray-session", file("spray-session")).
    dependsOn(common).
    settings(moduleSettings: _*).
    settings(libraryDependencies ++= Dependencies.session)

  lazy val login = Project("spray-login", file("spray-login")).
    dependsOn(common, session).
    settings(moduleSettings: _*).
    settings(libraryDependencies ++= Dependencies.login)

  lazy val oauth2 = Project("spray-oauth2", file("spray-oauth2")).
    dependsOn(common, data, session, login).
    settings(moduleSettings: _*).
    settings(libraryDependencies ++= Dependencies.oauth2)

  lazy val examples = Project("examples", file("examples")).
    aggregate(dataExample, sessionExample, oauth2Example).
    settings(exampleSettings: _*)

  lazy val dataExample = Project("spray-data-example", file("examples/spray-data")).
    dependsOn(common, data).
    settings(exampleSettings: _*).
    settings(libraryDependencies ++= Dependencies.dataExample)

  lazy val sessionExample = Project("spray-session-example", file("examples/spray-session")).
    dependsOn(session).
    settings(exampleSettings: _*).
    settings(libraryDependencies ++= Dependencies.sessionExample)

  lazy val oauth2Example = Project("spray-oauth2-example", file("examples/spray-oauth2")).
    enablePlugins(SbtTwirl).
    dependsOn(oauth2, login, common).
    settings(exampleSettings: _*).
    settings(libraryDependencies ++= Dependencies.oauth2Example)

  override lazy val settings = super.settings ++ buildSettings ++ shellSettings ++ resolverSettings

  lazy val shellSettings = Seq(
    shellPrompt := { state => Project.extract(state).currentProject.id + " > " })

  lazy val resolverSettings = Seq(
    resolvers ++= Seq(
      Resolver.sonatypeRepo("snapshots"),
      "spray repo" at "http://repo.spray.io"))

  lazy val rootSettings = noPublishingSettings

  lazy val moduleSettings = scalaProjectSettings ++ Seq(crossPaths := false)

  lazy val exampleSettings = scalaProjectSettings ++ Seq(crossPaths := false) ++ noPublishingSettings

  lazy val scalaProjectSettings = scalariformSettings ++ scalacSettings

  lazy val scalariformSettings = SbtScalariform.scalariformSettings ++ Seq(
    ScalariformKeys.preferences in Compile := formattingPreferences,
    ScalariformKeys.preferences in Test := formattingPreferences)

  import scalariform.formatter.preferences._

  lazy val formattingPreferences = FormattingPreferences()
    .setPreference(RewriteArrowSymbols, true)
    .setPreference(AlignParameters, true)
    .setPreference(AlignSingleLineCaseStatements, true)
    .setPreference(DoubleIndentClassDeclaration, true)

  lazy val scalacSettings = Seq(
    scalacOptions := Seq(
      "-deprecation",
      "-encoding", "UTF-8",
      "-feature",
      "-target:jvm-1.7",
      "-unchecked",
      "-Xlint",
      "-Xlog-reflective-calls"))

  lazy val noPublishingSettings = Seq(
    publish := (),
    publishLocal := ())
}

object Dependencies {
  private def compile(dependencies: ModuleID*) = dependencies map (_ % "compile")
  private def runtime(dependencies: ModuleID*) = dependencies map (_ % "runtime")
  private def provided(dependencies: ModuleID*) = dependencies map (_ % "provided")
  private def test(dependencies: ModuleID*) = dependencies map (_ % "test")

  val akkaActor    = "com.typesafe.akka"                      %% "akka-actor"                  % "2.3.7"
  val akkaSlf4j    = "com.typesafe.akka"                      %% "akka-slf4j"                  % "2.3.7"
  val logback      = "ch.qos.logback"                         %  "logback-classic"             % "1.1.2"
  val pickling     = "org.scala-lang"                         %% "scala-pickling"              % "0.9.0"
  val sprayCan     = "io.spray"                               %% "spray-can"                   % "1.3.2"
  val sprayHttp    = "io.spray"                               %% "spray-http"                  % "1.3.2"
  val sprayRouting = "io.spray"                               %% "spray-routing-shapeless2"    % "1.3.2"
  val sprayJson    = "io.spray"                               %% "spray-json"                  % "1.3.1"
  val commonsCodec = "commons-codec"                          %  "commons-codec"               % "1.10"
  val parboiled2   = "org.parboiled"                          %% "parboiled"                   % "2.0.1"
  val clHashMap    = "com.googlecode.concurrentlinkedhashmap" %  "concurrentlinkedhashmap-lru" % "1.4"
  val scalaRedisNb = "net.debasishg"                          %% "redisreact"                  % "0.7"

  val scalaTest    = "org.scalatest"                          %% "scalatest"                   % "2.2.2"
  val akkaTestkit  = "com.typesafe.akka"                      %% "akka-testkit"                % "2.3.7"
  val sprayTestkit = "io.spray"                               %% "spray-testkit"               % "1.3.2"

  val common = compile(parboiled2, clHashMap) ++
    provided(akkaActor, sprayRouting, sprayJson) ++
    test(scalaTest, akkaTestkit, sprayTestkit)

  val data = compile(sprayRouting, parboiled2) ++
    provided(akkaActor, sprayJson) ++
    test(scalaTest, akkaTestkit, sprayTestkit)

  val session = compile(sprayRouting, commonsCodec) ++
    provided(akkaActor, pickling) ++
    test(scalaTest, akkaTestkit, sprayTestkit)

  val login = compile(sprayRouting) ++
    provided(akkaActor) ++
    test(scalaTest, akkaTestkit, sprayTestkit)

  val oauth2 = compile(sprayRouting, sprayJson, pickling) ++
    provided(akkaActor, scalaRedisNb) ++
    test(scalaTest, akkaTestkit, sprayTestkit)

  val dataExample = compile(akkaActor, sprayCan, sprayJson) ++
    runtime(akkaSlf4j, logback)

  val sessionExample = compile(akkaActor, sprayCan) ++
    runtime(akkaSlf4j, logback)

  val oauth2Example = compile(akkaActor, sprayCan) ++
    runtime(akkaSlf4j, logback)
}
